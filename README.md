# PayKash 💰

## 🗂 Índice

- [Sobre o Projeto](#-sobre-o-projeto)
- [Instalação](#-instalação)
- [Utilização](#-utilização)
- [Tecnologias](#-tecnologias)

## 💻 Sobre o Projeto

Este projeto trata-se de uma versão simplificada de um marketplace e um sistema de pagamentos. Seu objetivo é ser uma aplicação onde é possível comprar produtos e processar transações. 

No momento, essa aplicação é possível cadastrar usuários de 3 categorias diferentes: administradores, vendedores e compradores. Os usuários vendedores podem cadastrar produtos enquanto os usuários compradores podem criar métodos de pagamento no sistema. Os usuários administradores podem cadastrar taxas no sistema.

> Status do Projeto: Em desenvolvimento ⚠️

## ⚙️ Instalação

> Este projeto requer que o [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) e o [Python](https://www.python.org/downloads/) estejam instalados em sua máquina.

- Faça o fork desse repositório;

- Abra o terminal e clone o repositório:

```bash
$ git clone git@gitlab.com:<your_user>/paykash.git
```

- Entre no diretório do projeto

```bash
$ cd paykash
```

- Crie um ambiente virtual:

```bash
$ python3 -m venv venv
```

- Ative o ambiente virtual:

```bash
$ source venv/bin/activate
```

- Faça a instalação das dependências:

```bash
$ pip install -r requirements.txt
```

- Execute as _migrations_ para criar o banco de dados e tabelas:

```bash
$ python3 manage.py migrate
```

- Execute a aplicação:

```bash
$ python3 manage.py runserver
```

Pronto! A aplicação, agora, pode ser acessada através da rota [http://localhost:8000/](http://localhost:8000/).

## 🚀 Utilização

> Para utilizar a aplicação, é necessário o uso de uma API Client, como [Postman](https://www.postman.com/) ou [Insomnia](https://insomnia.rest/download).

### Rotas

#### Criação de usuário: `POST - /api/accounts/`

_Requisição:_

```json
{
    "email": "jose@bol.com",
    "password": "12345678",
    "first_name": "Jose",
    "last_name": "Gaspar",
    "is_seller": true,
    "is_admin": false
}
```

_Resposta:_ **STATUS 201 - CREATED**

```json
{
    "id": "123e4567-e89b-12d3-a456-426655440000",
    "email": "jose@bol.com",
    "first_name": "Jose",
    "last_name": "Gaspar",
    "is_seller": true,
    "is_admin": false
}
```

#### Login de usuário: `POST - /api/login/`

_Requisição:_

```json
{
    "email": "jose@bol.com",
    "password": "12345678"
}
```

_Resposta:_ **STATUS 200 - OK**

```json
{
    "token": "1dad323aff679b208a33dea38513c4e5b14ffa4c"
}
```

#### Listagem de usuários: `GET - /api/accounts/`

_Requisição:_ **Sem body**

**Header - Authorization: Token <token_do_admin>**

_Resposta:_ **STATUS 200 - OK**

```json
[
    {
        "id": "123e4567-e89b-12d3-a456-426655440000",
        "email": "jose@bol.com",
        "first_name": "Jose",
        "last_name": "Gaspar",
        "is_seller": true,
        "is_admin": false
    },
    {
        "id": "123e4567-e89b-12d3-a456-426655440000",
        "email": "joao@hotmail.com",
        "first_name": "Joao",
        "last_name": "Souza",
        "is_seller": false,
        "is_admin": false
    },
    {
        "id": "123e4567-e89b-12d3-a456-426655440000",
        "email": "maria@gmail.com",
        "first_name": "Maria",
        "last_name": "Santos",
        "is_seller": false,
        "is_admin": true
    },
]
```

#### Criação de taxa: `POST - /api/fee/`

_Requisição:_
**Header - Authorization: Token <token_do_admin>**

```json
{
    "credit_fee": 0.06,
    "debit_fee": 0.04,
}
```

_Resposta:_ **STATUS 201 - CREATED**

```json
{
    "id": "123e4567-e89b-12d3-a456-426655440000",
    "credit_fee": 0.06,
    "debit_fee": 0.04,
    "created_at": "2022-02-18T19:51:18.052091Z"
}
```

#### Listagem de taxas: `GET - /api/fee/`

_Requisição:_ **Sem body**

**Header - Authorization: Token <token_do_admin>**

_Resposta:_ **STATUS 200 - OK**

```json
[
    {
        "id": "123e4567-e89b-12d3-a456-426655440000",
        "credit_fee": 0.06,
        "debit_fee": 0.04,
        "created_at": "2022-02-18T19:51:18.052091Z"
    },
    {
        "id": "eb3f73bd-fda4-4b80-a19f-34690fdc5124",
        "credit_fee": 0.08,
        "debit_fee": 0.02,
        "created_at": "2022-02-18T19:52:18.566114Z"
    },
    {
        "id": "7315fa3a-a74a-4bd3-9021-c923bb1e300c",
        "credit_fee": 0.04,
        "debit_fee": 0.01,
        "created_at": "2022-02-18T19:51:18.052091Z"
    }
]
```

#### Filtragem de taxa: `GET - /api/fee/<fee_id>/`

_Requisição:_ **Sem body**

**Header - Authorization: Token <token_do_admin>**

_Resposta:_ **STATUS 200 - OK**

```json
{
    "id": "123e4567-e89b-12d3-a456-426655440000",
    "credit_fee": 0.06,
    "debit_fee": 0.04,
    "created_at": "2022-02-18T19:51:18.052091Z"
}
```

#### Criação de produto: `POST - /api/products/`

_Requisição:_
**Header - Authorization: Token <token_do_vendedor>**

```json
{
    "description": "Smartband XYZ 3.0",
    "price": 100.99,
    "quantity": 15
}
```

_Resposta:_ **STATUS 201 - CREATED**

```json
{
    "id": "123e4567-e89b-12d3-a456-4266554400456",
    "seller": {
        "id": "123e4567-e89b-12d3-a456-426655440000",
        "email": "jose@bol.com",
        "first_name": "Jose",
        "last_name": "Gaspar",
        "is_seller": true,
        "is_admin": false
    },
    "description": "Smartband XYZ 3.0",
    "price": 100.99,
    "quantity": 15,
    "is_active": true,
}
```

#### Listagem de produtos: `GET - /api/products/`

_Requisição:_ **Sem body**

_Resposta:_ **STATUS 200 - OK**

```json
[
    {
        "id": "123e4567-e89b-12d3-a456-4266554400456"
        "description": "Smartband XYZ 3.0",
        "price": 100.99,
        "quantity": 15,
        "seller": "123e4567-e89b-12d3-a456-426655440000"
    },
    {
        "id": "123e4567-e89b-12d3-a456-4266554400123"
        "description": "Smartband ABC 3.0",
        "price": 300.99,
        "quantity": 18,
        "seller": "123f4567-e89b-12d3-a456-4266554400a2c"
    },
    {
        "id": "123e4567-e89b-12d3-a456-4266554400789"
        "description": "Smartband DEF 3.0",
        "price": 899.99,
        "quantity": 96,
        "seller": "123e4567-e90b-12d3-a456-4266554404bd"
    }
]
```

#### Filtragem de produto pelo id: `GET /api/products/<product_id>/`

_Requisição:_ **Sem body**

_Resposta:_ **STATUS 200 - OK**

```json
{
    "id": "123e4567-e89b-12d3-a456-4266554400456",
    "description": "Smartband XYZ 3.0",
    "price": 100.99,
    "quantity": 15,
    "is_active": true,
    "seller": "123e4567-e89b-12d3-a456-426655440000"
}
```

#### Atualização de produto: `PATCH - /api/products/<product_id>/`

_Requisição:_

**Header - Authorization: Token <token_do_vendedor>**

```json
{
    "description": "Smartband XYZ 3.0", // Campo opcional
    "price": 200.99, // Campo opcional
    "quantity": 20 // Campo opcional
    "is_active": false // Campo opcional
}
```

_Resposta:_ **STATUS 200 - OK**

```json
{
    "id": "123e4567-e89b-12d3-a456-4266554400456"
    "description": "Smartband XYZ 3.0",
    "price": 200.99,
    "quantity": 20,
    "is_active": false,
    "seller": "123e4567-e89b-12d3-a456-426655440000"
}
```

#### Filtragem de produto pelo vendedor: `GET - /api/products/seller/<seller_id>/`

_Requisição:_ **Sem body**

_Resposta:_ **STATUS 200 - OK**

```json
[
    {
        "id": "123e4567-e89b-12d3-a456-4266554400456"
        "description": "Smartband XYZ 3.0",
        "price": 100.99,
        "quantity": 15,
        "seller": "123e4567-e89b-12d3-a456-426655440000"
    },
    {
        "id": "123e4567-e89b-12d3-a456-4266554400789"
        "description": "Smartband ABCF 3.0",
        "price": 330.99,
        "quantity": 10,
        "seller": "123e4567-e89b-12d3-a456-426655440000"
    },
    {
        "id": "123e4567-e89b-12d3-a456-4266554400158"
        "description": "Smartband XPTO 3.0",
        "price": 899.99,
        "quantity": 17,
        "seller": "123e4567-e89b-12d3-a456-426655440000"
    }
]
```

#### Criação de método de pagamento: `POST - /api/payment_info/`

_Requisição:_

**Header - Authorizattion: Token <token_do_comprador>**

```json
{
    "payment_method": "debit",
    "card_number": "1234567812345678",
    "cardholders_name": "MARIANA F SOUZA",
    "card_expiring_date": "2022-04-01",
    "cvv": 456
}
```

_Resposta:_ **STATUS 201 - CREATED**

```json
{
    "id": "123e4567-e89b-12d3-a456-426654560000",
    "payment_method": "debit",
    "card_number_info": "5678",
    "cardholders_name": "MARIANA F SOUZA",
    "card_expiring_date": "2022-04-01",
    "is_active": true,
    "customer": "576580de-07d9-419a-9693-cd082a8cd659"
}
```

#### Listagem dos métodos de pagamento: `GET - /api/payment_info/`

_Requisição:_

**Header - Authorizattion: Token <token_do_comprador>**

_Resposta:_ **STATUS 200 - OK**

```json
[
    {
        "id": "123e4567-e89b-12d3-a456-426654560000",
        "payment_method": "debit",
        "card_number_info": "5678",
        "cardholders_name": "MARIANA F SOUZA",
        "card_expiring_date": "2022-04-01",
        "is_active": true,
        "customer": "576580de-07d9-419a-9693-cd082a8cd659"
    },
    {
        "id": "afcf6b96-7c72-4caa-8df2-0db1cf746542",
        "card_number_info": "4785",
        "payment_method": "CREDIT",
        "cardholders_name": "MARIANA F SOUZA",
        "card_expiring_date": "2028-12-01",
        "is_active": true,
        "customer": "576580de-07d9-419a-9693-cd082a8cd659"
    }
]
```

## 🛠 Tecnologias

Para o desenvolvimento desse projeto, foi necessária a utilização das seguintes ferramentas:

- [Django](https://www.djangoproject.com/)
- [Django Rest Framework](https://www.django-rest-framework.org/)
- [Python](https://www.python.org/)

---

Desenvolvido por Marcos Kenji Kuribayashi 😉
