from rest_framework.generics import ListCreateAPIView
from account.models import User
from payment_info.models import PaymentInfo
from payment_info.serializers import PaymentInfoSerializer
from rest_framework.authentication import TokenAuthentication
from payment_info.permissions import IsCustomer
from django.shortcuts import get_object_or_404


class PaymentInfoListCreateView(ListCreateAPIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsCustomer]
    
    queryset = PaymentInfo.objects.all()

    serializer_class = PaymentInfoSerializer


    def perform_create(self, serializer):
        
        serializer = serializer.save(customer=self.request.user)

        return serializer


    def get_queryset(self):

        customer = get_object_or_404(User, id=self.request.user.id)

        queryset = PaymentInfo.objects.filter(customer=customer)

        return queryset

