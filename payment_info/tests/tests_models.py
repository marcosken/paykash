from django.test import TestCase
from account.models import User
from payment_info.models import PaymentInfo
from datetime import date
import uuid


class PaymentInfoModelTest(TestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.email = "jose@bol.com"
        cls.password = "12345678"
        cls.first_name = "Jose"
        cls.last_name = "Gaspar"
        cls.is_seller = False
        cls.is_admin = False

        cls.user = User.objects.create_user(
            email=cls.email,
            password=cls.password,
            first_name=cls.first_name,
            last_name=cls.last_name,
            is_seller=cls.is_seller,
            is_admin=cls.is_admin,
        )

        cls.payment_method = "debit"
        cls.card_number = "1234567812345678"
        cls.cardholders_name = "MARIANA F SOUZA"
        cls.card_expiring_date = date.fromisoformat("2022-04-01")
        cls.cvv = 456

        cls.payment_info = PaymentInfo.objects.create(
            payment_method=cls.payment_method,
            card_number=cls.card_number,
            cardholders_name=cls.cardholders_name,
            card_expiring_date=cls.card_expiring_date,
            cvv=cls.cvv,
            customer=cls.user
        )


    def test_payment_info_fields(self):
        self.assertIsInstance(self.payment_info.id, uuid.UUID)

        self.assertIsInstance(self.payment_info.payment_method, str)
        self.assertEqual(self.payment_info.payment_method, self.payment_method)

        self.assertIsInstance(self.payment_info.card_number, str)
        self.assertEqual(self.payment_info.card_number, self.card_number)

        self.assertIsInstance(self.payment_info.cardholders_name, str)
        self.assertEqual(self.payment_info.cardholders_name, self.cardholders_name)

        self.assertIsInstance(self.payment_info.card_expiring_date, date)
        self.assertEqual(self.payment_info.card_expiring_date, self.card_expiring_date)

        self.assertIsInstance(self.payment_info.cvv, int)
        self.assertEqual(self.payment_info.cvv, self.cvv)

        self.assertIsInstance(self.payment_info.customer, User)
        self.assertEqual(self.payment_info.customer.email, self.user.email)
