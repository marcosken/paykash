from rest_framework.test import APITestCase
from account.models import User
from rest_framework.authtoken.models import Token


class PaymentInfoViewTest(APITestCase):

    @classmethod
    def setUpTestData(cls) -> None:
        cls.customer_user_data = {
            "email": "jose@bol.com",
            "password": "12345678",
            "first_name": "Jose",
            "last_name": "Gaspar"
        }

        cls.customer_user = User.objects.create_user(**cls.customer_user_data)

        cls.customer_token = Token.objects.create(user=cls.customer_user)

        cls.non_customer_user_data = {
            "email": "joana@bol.com",
            "password": "87654321",
            "first_name": "Joana",
            "last_name": "Santos",
            "is_seller": True
        }

        cls.non_customer_user = User.objects.create_user(**cls.non_customer_user_data)

        cls.non_customer_token = Token.objects.create(user=cls.non_customer_user)

        cls.invalid_token = "AaBb"

        cls.payment_info_data = {
            "payment_method": "debit",
            "card_number": "1234567812345678",
            "cardholders_name": "JOSE GASPAR",
            "card_expiring_date": "2022-04-01",
            "cvv": 456
        }

    
    def test_create_payment_info_success(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.customer_token}')

        response = self.client.post('/api/payment_info/', self.payment_info_data)

        self.assertEqual(response.status_code, 201)
        self.assertIn('card_number_info', response.json())
        self.assertNotIn('card_number', response.json())
        self.assertNotIn('cvv', response.json())
        self.assertIn('customer', response.json())
        self.assertEqual(response.data['customer'], self.customer_user.id)


    def test_create_payment_info_with_invalid_token(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.invalid_token}')

        response = self.client.post('/api/payment_info/', self.payment_info_data)

        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json(), {'detail': 'Invalid token.'})


    def test_create_product_without_admin_permission(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.non_customer_token}')

        response = self.client.post('/api/payment_info/', self.payment_info_data)

        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {'detail': 'You do not have permission to perform this action.'})


    def test_create_payment_info_with_expired_card(self):
        payment_info_data_expired_card = {
            "payment_method": "credit",
            "card_number": "1234567812345678",
            "cardholders_name": "JOSE GASPAR",
            "card_expiring_date": "2000-04-01",
            "cvv": 456
        }

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.customer_token}')

        response = self.client.post('/api/payment_info/', payment_info_data_expired_card)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {'error': ["This card is expired"]})


    def test_create_duplicated_payment_info(self):
        duplicated_payment_info_data = {
            "payment_method": "debit",
            "card_number": "1234567812345678",
            "cardholders_name": "JOSE GASPAR",
            "card_expiring_date": "2025-04-01",
            "cvv": 456
        }

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.customer_token}')

        self.client.post('/api/payment_info/', self.payment_info_data)

        response = self.client.post('/api/payment_info/', duplicated_payment_info_data)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {'error': ['This card is already registered for this user']})


    def test_list_payment_infos_success(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.customer_token}')

        self.client.post('/api/payment_info/', self.payment_info_data)

        response = self.client.get('/api/payment_info/')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 1)

    
    def test_list_payment_infos_with_invalid_token(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.invalid_token}')

        response = self.client.get('/api/payment_info/')

        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json(), {'detail': 'Invalid token.'})


    def test_list_payment_infos_without_admin_permission(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.non_customer_token}')

        response = self.client.get('/api/payment_info/')

        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {'detail': 'You do not have permission to perform this action.'})
