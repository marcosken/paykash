from django.urls import path
from payment_info.views import PaymentInfoListCreateView

urlpatterns = [
    path('payment_info/', PaymentInfoListCreateView.as_view())
]