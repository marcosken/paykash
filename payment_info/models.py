from django.db import models
import uuid


class PaymentInfo(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    payment_method = models.CharField(max_length=255)

    card_number = models.CharField(max_length=255)

    cardholders_name = models.CharField(max_length=255)

    card_expiring_date = models.DateField()

    cvv = models.CharField(max_length=3)

    is_active = models.BooleanField(default=True)

    customer = models.ForeignKey('account.User', on_delete=models.CASCADE, related_name='payment_methods')