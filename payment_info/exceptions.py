from rest_framework.exceptions import APIException


class ExpiredCardError(APIException):
    status_code = 400
    default_detail = {'error': ['This card is expired']}


class PaymentInfoAlreadyExistsError(APIException):
    status_code = 400
    default_detail = {'error': ['This card is already registered for this user']}
