from rest_framework import serializers
from payment_info.models import PaymentInfo
from datetime import date
from payment_info.exceptions import ExpiredCardError, PaymentInfoAlreadyExistsError


class PaymentInfoSerializer(serializers.ModelSerializer):

    card_number_info = serializers.SerializerMethodField()

    class Meta:
        model = PaymentInfo

        fields = '__all__'

        read_only_fields = ['id', 'customer']

        extra_kwargs = {'card_number': {'write_only': True}, 'cvv': {'write_only': True}}


    def validate(self, attrs):

        card_expiring_date = attrs['card_expiring_date']

        if card_expiring_date < date.today():
            raise ExpiredCardError()

        payment_method = attrs['payment_method']
        card_number = attrs['card_number']

        payment_info = PaymentInfo.objects.filter(payment_method=payment_method, card_number=card_number, customer=self.context['request'].user).first()

        if payment_info:
            raise PaymentInfoAlreadyExistsError()


        return super().validate(attrs)


    def get_card_number_info(self, obj):
        return obj.card_number[-4::]
        