from rest_framework import serializers
from fee.models import Fee


class FeeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Fee

        fields = '__all__'

        read_only_fields = ['id', 'created_at']
