from django.test import TestCase
from fee.models import Fee
from datetime import datetime
import uuid


class FeeModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.credit_fee = 0.06
        cls.debit_fee = 0.04

        cls.fee = Fee.objects.create(
            credit_fee=cls.credit_fee,
            debit_fee=cls.debit_fee,
        )

    
    def test_fee_fields(self):
        self.assertIsInstance(self.fee.id, uuid.UUID)
        
        self.assertIsInstance(self.fee.credit_fee, float)
        self.assertEqual(self.fee.credit_fee, self.credit_fee)

        self.assertIsInstance(self.fee.debit_fee, float)
        self.assertEqual(self.fee.debit_fee, self.debit_fee)

        self.assertIsInstance(self.fee.created_at, datetime)
