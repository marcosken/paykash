from rest_framework.test import APITestCase
from account.models import User
from fee.models import Fee
from rest_framework.authtoken.models import Token


class FeeViewTest(APITestCase):

    @classmethod
    def setUpTestData(cls) -> None:
        cls.admin_user_data = {
            "email": "jose@bol.com",
            "password": "12345678",
            "first_name": "Jose",
            "last_name": "Gaspar",
            "is_admin": True
        }

        cls.admin_user = User.objects.create_user(**cls.admin_user_data)

        cls.admin_token = Token.objects.create(user=cls.admin_user)

        cls.non_admin_user_data = {
            "email": "joana@bol.com",
            "password": "87654321",
            "first_name": "Joana",
            "last_name": "Santos",
            "is_admin": False
        }

        cls.non_admin_user = User.objects.create_user(**cls.non_admin_user_data)

        cls.non_admin_token = Token.objects.create(user=cls.non_admin_user)

        cls.invalid_token = "AaBb"

        cls.fee_data = {
            "credit_fee": 0.06,
            "debit_fee": 0.04
        }

    def test_create_fee_success(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.admin_token}')

        response = self.client.post('/api/fee/', self.fee_data)

        self.assertEqual(response.status_code, 201)
        self.assertIn('created_at', response.json())


    def test_create_fee_with_invalid_token(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.invalid_token}')

        response = self.client.post('/api/fee/', self.fee_data)

        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json(), {'detail': 'Invalid token.'})


    def test_create_fee_without_admin_permission(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.non_admin_token}')

        response = self.client.post('/api/fee/', self.fee_data)

        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {'detail': 'You do not have permission to perform this action.'})


    def test_list_fees_success(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.admin_token}')

        self.client.post('/api/fee/', self.fee_data)

        response = self.client.get('/api/fee/')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 2)


    def test_list_fees_with_invalid_token(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.invalid_token}')

        response = self.client.get('/api/fee/')

        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json(), {'detail': 'Invalid token.'})


    def test_list_fees_without_admin_permission(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.non_admin_token}')

        response = self.client.get('/api/fee/')

        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {'detail': 'You do not have permission to perform this action.'})


    def test_retrieve_a_specific_fee_success(self):
        fee = Fee.objects.create(**self.fee_data)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.admin_token}')

        response = self.client.get(f'/api/fee/{fee.id}/')

        self.assertEqual(response.status_code, 200)
        self.assertIn('created_at', response.json())


    def test_retrieve_a_specific_fee_with_invalid_token(self):
        fee = Fee.objects.create(**self.fee_data)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.invalid_token}')

        response = self.client.get(f'/api/fee/{fee.id}/')

        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json(), {'detail': 'Invalid token.'})

    
    def test_retrieve_a_specific_fee_without_admin_permission(self):
        fee = Fee.objects.create(**self.fee_data)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.non_admin_token}')

        response = self.client.get(f'/api/fee/{fee.id}/')

        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {'detail': 'You do not have permission to perform this action.'})
