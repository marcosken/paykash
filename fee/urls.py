from django.urls import path
from fee.views import FeeListCreateView, FeeRetrieveView


urlpatterns = [
    path('fee/', FeeListCreateView.as_view()),
    path('fee/<str:fee_id>/',  FeeRetrieveView.as_view())
]