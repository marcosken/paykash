from rest_framework.generics import ListCreateAPIView, RetrieveAPIView
from fee.models import Fee
from fee.serializers import FeeSerializer
from rest_framework.authentication import TokenAuthentication
from fee.permissions import IsAdmin


class FeeListCreateView(ListCreateAPIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAdmin]

    queryset = Fee.objects.all()

    serializer_class = FeeSerializer


class FeeRetrieveView(RetrieveAPIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAdmin]

    queryset = Fee.objects.all()

    serializer_class = FeeSerializer

    lookup_url_kwarg = 'fee_id'
