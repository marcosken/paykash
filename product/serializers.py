from rest_framework import serializers
from product.models import Product
from account.serializers import UserSerializer


class ProductSerializer(serializers.ModelSerializer):

    seller = UserSerializer(read_only=True)

    class Meta:
        model = Product

        fields = '__all__'

        read_only_fields = ['is_active']


    def create(self, validated_data):
        
        if validated_data['quantity'] == 0:
            validated_data['is_active'] = False

        return super().create(validated_data)


class BasicProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product

        fields = '__all__'
