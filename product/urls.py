from django.urls import path
from product.views import ProductListCreateView, ProductRetrieveUpdateView, ProductRetrieveBySellerView

urlpatterns = [
    path('products/', ProductListCreateView.as_view()),
    path('products/<str:product_id>/', ProductRetrieveUpdateView.as_view()),
    path('products/seller/<str:seller_id>/', ProductRetrieveBySellerView.as_view())
]