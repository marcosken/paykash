import uuid
from django.test import TestCase
from account.models import User
from product.models import Product


class ProductModelTest(TestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.email = "jose@bol.com"
        cls.password = "12345678"
        cls.first_name = "Jose"
        cls.last_name = "Gaspar"
        cls.is_seller = True
        cls.is_admin = False

        cls.user = User.objects.create_user(
            email=cls.email,
            password=cls.password,
            first_name=cls.first_name,
            last_name=cls.last_name,
            is_seller=cls.is_seller,
            is_admin=cls.is_admin,
        )

        cls.description = "Smartband XYZ 3.0"
        cls.price = 100.99
        cls.quantity = 15

        cls.product = Product.objects.create(
            description=cls.description,
            price=cls.price,
            quantity=cls.quantity,
            seller=cls.user
        )


    def test_product_fields(self):
        self.assertIsInstance(self.product.id, uuid.UUID)

        self.assertIsInstance(self.product.description, str)
        self.assertEqual(self.product.description, self.description)

        self.assertIsInstance(self.product.price, float)
        self.assertEqual(self.product.price, self.price)

        self.assertIsInstance(self.product.quantity, int)
        self.assertEqual(self.product.quantity, self.quantity)

        self.assertIsInstance(self.product.is_active, bool)
        self.assertEqual(self.product.seller.is_active, True)

        self.assertIsInstance(self.product.seller, User)
        self.assertEqual(self.product.seller.email, self.email)
        