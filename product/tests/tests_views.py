from rest_framework.test import APITestCase
from account.models import User
from product.models import Product
from rest_framework.authtoken.models import Token


class ProductViewTest(APITestCase):

    @classmethod
    def setUpTestData(cls) -> None:
        cls.seller_user_data = {
            "email": "jose@bol.com",
            "password": "12345678",
            "first_name": "Jose",
            "last_name": "Gaspar",
            "is_seller": True
        }

        cls.seller_user = User.objects.create_user(**cls.seller_user_data)

        cls.seller_token = Token.objects.create(user=cls.seller_user)

        cls.non_seller_user_data = {
            "email": "joana@bol.com",
            "password": "87654321",
            "first_name": "Joana",
            "last_name": "Santos",
            "is_seller": False
        }

        cls.non_seller_user = User.objects.create_user(**cls.non_seller_user_data)

        cls.non_seller_token = Token.objects.create(user=cls.non_seller_user)

        cls.invalid_token = "AaBb"

        cls.product_data = {
            "description": "Smartband XYZ 3.0",
            "price": 100.99,
            "quantity": 15
        }

        cls.wrong_product_id = "AbCdEf"

        cls.payload = {
            "description": "Smartband XYZ 3.0",
            "price": 200.99,
            "quantity": 20,
            "is_active": False
        }

    def test_create_product_success(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.seller_token}')

        response = self.client.post('/api/products/', self.product_data)

        self.assertEqual(response.status_code, 201)
        self.assertIn('seller', response.json())

    
    def test_create_product_with_invalid_token(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.invalid_token}')

        response = self.client.post('/api/products/', self.product_data)

        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json(), {'detail': 'Invalid token.'})


    def test_create_product_without_admin_permission(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.non_seller_token}')

        response = self.client.post('/api/products/', self.product_data)

        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {'detail': 'You do not have permission to perform this action.'})


    def test_list_products_success(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.seller_token}')

        self.client.post('/api/products/', self.product_data)

        response = self.client.get('/api/products/')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 1)


    def test_retrieve_a_specific_product_success(self):
        product = Product.objects.create(**self.product_data, seller=self.seller_user)

        response = self.client.get(f'/api/products/{product.id}/')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['seller'], self.seller_user.id)


    def test_retrieve_a_non_existing_product_id(self):
        response = self.client.get(f'/api/products/{self.wrong_product_id}/')

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {'detail': 'Not found.'})


    def test_update_a_specific_product_success(self):
        product = Product.objects.create(**self.product_data, seller=self.seller_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.seller_token}')

        response = self.client.patch(f'/api/products/{product.id}/', self.payload)

        product.refresh_from_db()

        self.assertEqual(response.status_code, 200)
        
        for key, value in self.payload.items():
            self.assertEqual(response.data[key], value)
            self.assertEqual(getattr(product, key), value)


    def test_update_a_non_existing_product_id(self):
        product = Product.objects.create(**self.product_data, seller=self.seller_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.seller_token}')

        response = self.client.patch(f'/api/products/{self.wrong_product_id}/', self.payload)

        product.refresh_from_db()

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {'detail': 'Not found.'})

        self.assertNotEqual(product.price, self.payload['price'])
        self.assertNotEqual(product.quantity, self.payload['quantity'])


    def test_update_a_specific_product_with_invalid_token(self):
        product = Product.objects.create(**self.product_data, seller=self.seller_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.invalid_token}')

        response = self.client.patch(f'/api/fee/{product.id}/', self.payload)

        product.refresh_from_db()

        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json(), {'detail': 'Invalid token.'})

        self.assertNotEqual(product.price, self.payload['price'])
        self.assertNotEqual(product.quantity, self.payload['quantity'])


    def test_update_a_specific_fee_without_admin_permission(self):
        product = Product.objects.create(**self.product_data, seller=self.seller_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.non_seller_token}')

        response = self.client.patch(f'/api/fee/{product.id}/', self.payload)

        product.refresh_from_db()

        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {'detail': 'You do not have permission to perform this action.'})

        self.assertNotEqual(product.price, self.payload['price'])
        self.assertNotEqual(product.quantity, self.payload['quantity'])


    def test_update_a_specific_product_that_does_not_belong_to_the_user_doing_this_action(self):
        seller_user_two = User.objects.create(
            email="ana@bol.com",
            password="12345678",
            first_name="Ana",
            last_name="Nascimento",
            is_seller=True
        )

        product = Product.objects.create(**self.product_data, seller=seller_user_two)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.seller_token}')

        response = self.client.patch(f'/api/fee/{product.id}/', self.payload)

        product.refresh_from_db()

        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {'detail': 'You do not have permission to perform this action.'})

        self.assertNotEqual(product.price, self.payload['price'])
        self.assertNotEqual(product.quantity, self.payload['quantity'])


    def test_filter_products_by_seller_id(self):
        products = [Product.objects.create(**self.payload, seller=self.seller_user) for _ in range(10)]

        response = self.client.get(f'/api/products/seller/{self.seller_user.id}/')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(products), len(response.json()))
