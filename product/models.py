from django.db import models
import uuid
from django.core.validators import MinValueValidator


class Product(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    description = models.TextField()

    price = models.FloatField()

    quantity = models.PositiveIntegerField(validators=[MinValueValidator(0)])

    is_active = models.BooleanField(default=True)

    seller = models.ForeignKey('account.User', on_delete=models.CASCADE, related_name='products')
