from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView, ListAPIView
from product.models import Product
from account.models import User
from product.serializers import ProductSerializer, BasicProductSerializer
from rest_framework.authentication import TokenAuthentication
from product.permissions import IsSeller, IsOwner
from django.shortcuts import get_object_or_404


class ProductListCreateView(ListCreateAPIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSeller]

    queryset = Product.objects.all()

    serializer_class = ProductSerializer


    def perform_create(self, serializer):

        serializer = serializer.save(seller=self.request.user)

        return serializer


    def get_serializer_class(self):
        
        if self.request.method == 'GET':
            return BasicProductSerializer
        
        return super().get_serializer_class()


class ProductRetrieveUpdateView(RetrieveUpdateAPIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSeller, IsOwner]

    queryset = Product.objects.all()

    serializer_class = ProductSerializer

    lookup_url_kwarg = 'product_id'


    def get_serializer_class(self):
        
        if self.request.method == 'GET' or self.request.method == 'PATCH':
            return BasicProductSerializer
        
        return super().get_serializer_class()


class ProductRetrieveBySellerView(ListAPIView):

    queryset = Product.objects.all()

    serializer_class = BasicProductSerializer

    lookup_url_kwarg = 'seller_id'


    def get_queryset(self):

        seller = get_object_or_404(User, id=self.kwargs['seller_id'])

        queryset = Product.objects.filter(seller=seller)

        return queryset
