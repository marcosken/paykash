from django.urls import path
from account.views import UserListCreateView, LoginView


urlpatterns = [
    path('accounts/', UserListCreateView.as_view()),
    path('login/', LoginView.as_view())
]