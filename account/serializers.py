from rest_framework import serializers
from account.models import User


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User

        fields = ['id', 'email', 'password', 'first_name', 'last_name', 'is_seller', 'is_admin']

        read_only_fields = ['id']

        extra_kwargs = {'password': {'write_only': True}}


    def create(self, validated_data):
        return User.objects.create_user(**validated_data)


class LoginSerializer(serializers.Serializer):

    email = serializers.EmailField()

    password = serializers.CharField()