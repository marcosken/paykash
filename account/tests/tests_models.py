from django.test import TestCase
from account.models import User
from product.models import Product
from payment_info.models import PaymentInfo
import uuid

class UserModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.email = "jose@bol.com"
        cls.password = "12345678"
        cls.first_name = "Jose"
        cls.last_name = "Gaspar"
        cls.is_seller = True
        cls.is_admin = False

        cls.user = User.objects.create_user(
            email=cls.email,
            password=cls.password,
            first_name=cls.first_name,
            last_name=cls.last_name,
            is_seller=cls.is_seller,
            is_admin=cls.is_admin,
        )


    def test_user_fields(self):
        self.assertIsInstance(self.user.id, uuid.UUID)

        self.assertIsInstance(self.user.email, str)
        self.assertEqual(self.user.email, self.email)

        self.assertIsInstance(self.user.password, str)

        self.assertIsInstance(self.user.first_name, str)
        self.assertEqual(self.user.first_name, self.first_name)

        self.assertIsInstance(self.user.last_name, str)
        self.assertEqual(self.user.last_name, self.last_name)

        self.assertIsInstance(self.user.is_seller, bool)
        self.assertEqual(self.user.is_seller, self.is_seller)

        self.assertIsInstance(self.user.is_admin, bool)
        self.assertEqual(self.user.is_admin, self.is_admin)
        

    def test_seller_user_may_contain_multiple_products(self):
        products = [
            Product.objects.create(
                description="Smartband XYZ 3.0",
                price=109.99,
                quantity=15,
                seller=self.user
            ) for _ in range(5)
        ]

        self.assertEqual(len(products), self.user.products.count())

        for product in products:
            self.assertIs(product.seller, self.user)

    
    def test_customer_user_may_contain_multiple_payment_infos(self):
        payment_info_one = PaymentInfo.objects.create(
            payment_method="debit",
            card_number="12345678",
            cardholders_name="JOSE GASPAR",
            card_expiring_date="2022-04-01",
            cvv=456,
            customer=self.user
        )

        payment_info_two = PaymentInfo.objects.create(
            payment_method="credit",
            card_number="87654321",
            cardholders_name="JOSE GASPAR",
            card_expiring_date="2023-07-01",
            cvv=444,
            customer=self.user
        )

        self.assertEqual(self.user.payment_methods.count(), 2)

        self.assertIs(payment_info_one.customer, self.user)
        self.assertIs(payment_info_two.customer, self.user)
