from rest_framework.test import APITestCase
from account.models import User
from rest_framework.authtoken.models import Token


class UserViewTets(APITestCase):

    @classmethod
    def setUpTestData(cls) -> None:
        cls.user_data = {
            "email": "jose@bol.com",
            "password": "12345678",
            "first_name": "Jose",
            "last_name": "Gaspar",
            "is_seller": True,
            "is_admin": True
        }

    def test_create_user_success(self):
        response = self.client.post('/api/accounts/', self.user_data)

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json()['email'], self.user_data["email"])
        self.assertNotIn('password', response.json())
        self.assertEqual(response.json()['first_name'], self.user_data["first_name"])
        self.assertEqual(response.json()['last_name'], self.user_data["last_name"])
        self.assertEqual(response.json()['is_seller'], self.user_data["is_seller"])
        self.assertEqual(response.json()['is_admin'], self.user_data["is_admin"])


    def test_create_user_with_some_missing_required_field(self):
        wrong_user_data = {
            "email": "jose1@bol.com",
            "last_name": "Gaspar",
            "is_seller": True,
            "is_admin": True
        }

        response = self.client.post('/api/accounts/', wrong_user_data)

        self.assertEqual(response.status_code, 400)
        self.assertIn('password', response.json())
        self.assertIn('first_name', response.json())


    def test_creat_user_with_duplicate_email(self):
        self.client.post('/api/accounts/', self.user_data)

        response = self.client.post('/api/accounts/', self.user_data)

        self.assertEqual(response.status_code, 400)
        self.assertIn('email', response.json())


    def test_list_users_success(self):
        user = User.objects.create(**self.user_data)

        token = Token.objects.create(user=user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        response = self.client.get('/api/accounts/')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 1)
        


    def test_list_users_with_invalid_token(self):
        invalid_token = "AaBb"

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {invalid_token}')

        response = self.client.get('/api/accounts/')

        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json(), {'detail': 'Invalid token.'})


    def test_list_users_without_admin_permission(self):
        non_admin_user = User.objects.create(
            email="jose@bol.com",
            password="12345678",
            first_name="Jose",
            last_name="Gaspar",
            is_seller=False,
            is_admin=False
        )

        token = Token.objects.create(user=non_admin_user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

        response = self.client.get('/api/accounts/')

        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {'detail': 'You do not have permission to perform this action.'})


class LoginViewTest(APITestCase):
  
    def setUp(self) -> None:
        User.objects.create_user(
            email="jose@bol.com",
            password="12345678",
            first_name="Jose",
            last_name="Gaspar",
            is_seller=True,
            is_admin=False
        )


    def test_login_success(self):
        login_data = {
            "email": "jose@bol.com",
            "password": "12345678"
        }    

        response = self.client.post('/api/login/', login_data)

        self.assertEqual(response.status_code, 200)
        self.assertIn('token', response.json())



    def test_login_with_some_missing_required_field(self):
        login_data = {
            "email": "jose@bol.com",
        }

        response = self.client.post('/api/login/', login_data)

        self.assertEqual(response.status_code, 400)
        self.assertNotIn('token', response.json())

    
    def test_login_with_invalid_credentials(self):
        login_data = {
            "email": "jose@bol.com",
            "password": "1234"
        }

        response = self.client.post('/api/login/', login_data)

        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json(), {"error": "Invalid credentials."})

