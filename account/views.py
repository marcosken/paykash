from rest_framework.generics import ListCreateAPIView
from account.models import User
from account.serializers import UserSerializer, LoginSerializer
from rest_framework.views import APIView, Response, status
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication
from account.permissions import AdminListPermission


class UserListCreateView(ListCreateAPIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [AdminListPermission]

    queryset = User.objects.all()

    serializer_class = UserSerializer


class LoginView(APIView):

    def post(self, request):
        serializer = LoginSerializer(data=request.data)

        serializer.is_valid(raise_exception=True)

        user = authenticate(**serializer.validated_data)

        if user:
            token = Token.objects.get_or_create(user=user)[0]

            return Response({'token': token.key})
        
        return Response({'error': 'Invalid credentials.'}, status=status.HTTP_401_UNAUTHORIZED)
